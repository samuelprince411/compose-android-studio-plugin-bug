## How to reproduce the bug

1. Run command in terminal: `./gradlew build`, the generated jar can be found under build/distributions. Install this plugin jar in android studio.
2. From menu `Code` select `Show Compose Demo...`
3. View the stack trace in the IDE fatal error section at the bottom right corner
